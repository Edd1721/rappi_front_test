'use strict'

const express = require('express')
const bodyparser = require('body-parser')
const helmet = require('helmet')
const fetch = require('node-fetch')
const fs = require('fs')
const app = express()
const port = process.env.PORT || 3000

let readFile = true

app.engine('html', require('ejs').renderFile)
app.set('view engine', 'html')
app.set('views', `${__dirname}/public/views`)

app.use(bodyparser.json())
app.use(express.static(`${__dirname}/public`))
app.use(helmet())
app.use(bodyparser.urlencoded({
  extended: false
}))

app.get('/', (req, res) => {
  res.render('index')
})

app.get('/products', (req, res) => {
  if (readFile) {
    fs.readFile('./products.json', (err, data) => {
      if (err) {
        res.status(err.status || 404).send(err.message)
      }

      res.send(data)
    })
  } else {
    fetch('https://urltodata.com/')
      .then(function (result) {
        res.send(result.json())
      })
  }
})

app.listen(port, () => {
  console.log(`Servidor escuchando en el puerto ${port}`)
})
